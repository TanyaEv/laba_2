from search import func, spisok, LinearSearch, BinarySearch, NaiveSearch, KMP, points
from graphics import count
from random import choice

print("Выберите метод поиска, либо оценку сложности алгоритмов\n"
      "[1]Линейный\n"
      "[2]Бинарный\n"
      "[3]Наивный\n"
      "[4]Кнута-Морриса-Пратта\n"
      "[5]Оценка сложности")

num = int(input('>>> '))

if num == 1:
    print("Линейный поиск")
    size = int(input("Введите размер массива: "))
    elem = int(input("Шаблон поиска: "))
    print("---Результаты работы---")
    mass = func(size)
    index, count = LinearSearch(mass, elem)
    print(f'Индекс найденного шаблона: {index}\nКол-во итераций: {count}')

elif num == 2:
    print("Бинарный поиск")
    size = int(input("Введите размер массива: "))
    elem = int(input("Шаблон поиска: "))
    print("---Результаты работы---")
    mass = func(size)
    index, count = BinarySearch(sorted(mass), elem)
    print(f'Индекс найденного шаблона: {index}\nКол-во итераций: {count}')

elif num == 3:
    print("наивный поиск")
    size = int(input("Введите размер списка: "))
    elem = input("Шаблон поиска: ")
    print("---Результаты работы---")
    mass = spisok(size)
    index, count = NaiveSearch(mass, elem)
    print(f'Индекс найденного шаблона: {index}\nКол-во итераций: {count}')

elif num == 4:
    print("Поиск Кнута-Морриса-Пратта")
    size = int(input("Введите размер списка: "))
    elem = input("Шаблон поиска: ")
    print("---Результаты работы---")
    mass = spisok(size)
    index, count = KMP(mass, elem)
    print(f'Индекс найденного шаблона: {index}\nКол-во итераций: {count}')

elif num == 5:
    print('Оценка сложности')
    min_size = int(input("Введите минимальный размер списка/массива: "))
    max_size = int(input("Введите максимальный размер списка/массива:"))

    points = points(min_size, max_size)
    mass = [24, 26, 27, 47, 56, 34, 123, 54, 68, 90, 57]
    letters = ["AB", "AD", "AC", "FG"]

    count_linear, count_binary, count_naive, count_kms = [], [], [], []

    for i in range(min_size, max_size + 100, 100):
        arr_linear, count_l = LinearSearch(func(i), choice(mass))
        count_linear.append(count_l)

        arr_binary, count_b = BinarySearch(func(i), choice(mass))
        count_binary.append(count_b)

        arr_naive, count_n = NaiveSearch(spisok(i), choice(letters))
        count_naive.append(count_n)

        arr_kms, count_k_m_s = KMP(spisok(i), choice(letters))
        count_kms.append(count_k_m_s)

    count(points, count_linear, count_binary, count_naive, count_kms)
