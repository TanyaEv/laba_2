from random import choice


def func(size):
    mass = [24, 26, 27, 47, 56, 34, 123, 54, 68, 90, 57]
    return [choice(mass) for _ in range(size)]


def spisok(size):
    letters = ["AB", "AD", "AC", "FG"]
    string = ''.join(choice(letters) for _ in range(size))
    return string


def points(min_size, max_size):  # Подсчет кол-ва точек
    return [i for i in range(min_size, max_size + 100, 100)]


def LinearSearch(arr, element):  # Линейный поиск
    count = 0
    for i in range(len(arr)):
        if arr[i] == element:
            return i, count
        count += 1
    return None, count


def BinarySearch(array, elem):  # Бинарный поиск
    count = 0
    i = 0
    j = len(array) - 1
    while i < j:
        m = int((i + j) / 2)
        if elem > array[m]:
            count += 1
            i = m + 1
        else:
            j = m
    if array[j] == elem:
        return j, count
    else:
        return None, count


def NaiveSearch(text, part):  # Наивный поиск
    count = 0
    size = len(part)
    for i in range(len(text) - size + 1):
        count += 1
        if text[i: i + size] == part:
            return i, count


def KMP(text, pattern):  # поиск Кнута-Морриса-Пратта
    count = 0
    pattern = list(pattern)

    shifts = [1] * (len(pattern) + 1)
    shift = 1
    for pos in range(len(pattern)):
        while shift <= pos and pattern[pos] != pattern[pos - shift]:
            shift += shifts[pos - shift]
        shifts[pos + 1] = shift

    startPos = 0
    matchLen = 0
    for c in text:
        count += 1
        while matchLen == len(pattern) or \
                matchLen >= 0 and pattern[matchLen] != c:
            startPos += shifts[matchLen]
            matchLen -= shifts[matchLen]
        matchLen += 1
        if matchLen == len(pattern):
            return startPos, count

    return None, count
